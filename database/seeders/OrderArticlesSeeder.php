<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Order;
use Illuminate\Database\Seeder;

class OrderArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = Article::all();

        Order::all()->each(function ($order) use ($articles) {
            $articles = $articles->random(random_int(1,8));
            $sum = 0;
            foreach ($articles as $article) {
                $order->articles()->attach(
                    $article,
                    [
                        'quantity' => random_int(1,3),
                        'created_at' => now(),
                        'updated_at' => now(),
                        'unit_price' => $article->unit_price,
                    ]
                );
                $sum += $article->unit_price;
                $order->update(['total' => $sum]);
            }
        });
    }
}
