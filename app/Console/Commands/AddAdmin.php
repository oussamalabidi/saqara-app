<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class AddAdmin extends Command
{
    const ADMIN_EMAIL = "admin@test.com";
    const ADMIN_NAME = "admin";
    const ADMIN_PASSWORD = "admin";
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add user admin to connect to the admin';
    /**
     * @var User
     */
    private $user;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (0 === $this->user->where(["email" => self::ADMIN_EMAIL])->count()) {
            User::create(
                [
                    'name' => self::ADMIN_NAME,
                    'email' => self::ADMIN_EMAIL,
                    'password' => Hash::make(self::ADMIN_PASSWORD),
                    'is_admin' => true
                ]
            );
            $this->info('Admin was created successfully!');
        } else {
            $this->error('Admin already exist');
        }
    }
}
