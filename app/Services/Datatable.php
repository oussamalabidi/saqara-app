<?php


namespace App\Services;


use App\Models\Customer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Yajra\DataTables\DataTables;

class Datatable
{
    public function generateCustomers(Collection $customers)
    {
        return DataTables::of($customers)
            ->addIndexColumn()
            ->addColumn('email', function(Customer $customer) {

                return $customer->user->email;
            })
            ->addColumn('nOrders', function(Customer $customer) {
                return $customer->getOrdersCountAttribute();
            })
            ->make(true);
    }

    public function generateOrders(Collection $orders)
    {
        return DataTables::of($orders)
            ->addIndexColumn()
            ->addColumn('customer', function(Order $order) {
                return $order->customer->firstname . ' ' . $order->customer->lastname;
            })
            ->addColumn('details', function(Order $order){
                return $this->getOrderDetails($order);
            })
            ->addColumn('created_at', function(Order $order){
                return Carbon::make($order->created_at)->format('Y-m-d H:i:s');
            })
            ->rawColumns(['details'])
            ->make(true);
    }

    private function getOrderDetails(Order $order)
    {
        $response = '<ul>';
        foreach ($order->articles as $article) {
            $response .= '<li>' . $article->name .' / '.$article->reference .' / '.$article->unit_price.'$</li>';
        }
        $response .= '</ul>';
        return $response;
    }

}
