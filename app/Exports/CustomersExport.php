<?php

namespace App\Exports;

use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CustomersExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return Collection
    */
    public function collection()
    {
        return Customer::with('orders')->with('user')->get();
    }

    /**
     * @param mixed $customer
     * @return array
     */
    public function map($customer) : array {
        return [
            $customer->id,
            $customer->user->email,
            $customer->firstname,
            $customer->lastname,
            $customer->phone,
            $customer->getOrdersCountAttribute(),
            Carbon::parse($customer->created_at)->format('Y-m-d H:i:s')
        ] ;


    }

    /**
     * @return array
     */
    public function headings() : array {
        return [
            '#',
            'Email',
            'firstname',
            'lastname',
            'Phone',
            'N° orders',
            'Created At'
        ] ;
    }
}
