<?php


namespace App\Http\Constants;


class Error
{
    const LOGIN_FAILED = 'The user name or password are incorrect';
}
