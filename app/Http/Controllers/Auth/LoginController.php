<?php

namespace App\Http\Controllers\Auth;

use App\Http\Constants\Error;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt(
            [
                'email' => $request->get('email'),
                'password' => $request->get('password'),
                'is_admin' => 1
            ]
        )) {
            return redirect()->intended('dashboard');
        } else {
            session()->flash('error', Error::LOGIN_FAILED);
            return redirect()->route('home');
        }
    }

    /**
     * @return RedirectResponse
     */
    public function logout() {
        Auth::logout();
        return redirect()->route('home');
    }
}
