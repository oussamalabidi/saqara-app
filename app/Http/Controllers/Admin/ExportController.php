<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CustomersExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ExportController extends Controller
{
    const FILE_NAME = "clients-et-commandes.xlsx";

    /**
     * @return BinaryFileResponse
     */
    public function exportCustomers()
    {
        return Excel::download(new CustomersExport(), self::FILE_NAME);
    }
}
