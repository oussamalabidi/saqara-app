<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Services\Datatable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CustomersController extends Controller
{

    /**
     * @var Datatable
     */
    private $datatable;

    /**
     * CustomersController constructor.
     * @param Datatable $datatable
     */
    public function __construct(Datatable $datatable)
    {
        $this->datatable = $datatable;
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $customers = Customer::all();
            return $this->datatable->generateCustomers($customers);
        }

        return view('admin.customers');
    }

}
