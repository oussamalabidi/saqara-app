<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Services\Datatable;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * @var Datatable
     */
    private $datatable;

    public function __construct(Datatable $datatable)
    {
        $this->datatable = $datatable;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $orders = Order::all();
            return $this->datatable->generateOrders($orders);
        }

        return view('admin.orders');
    }
}
