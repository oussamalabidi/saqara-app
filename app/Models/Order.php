<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'order_articles');
    }

    public function getArticlesCountAttribute()
    {
        return $this->articles()->count();
    }
}
