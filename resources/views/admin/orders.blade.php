@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
@endsection
@section('content')

    <div class="row">
        <div class="container">
            <table class="table table-bordered data-table">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Customer</th>
                    <th>Total</th>
                    <th>Created at</th>
                    <th>Articles</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>


@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('orders') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'customer', name: 'customer'},
                    {data: 'total', name: 'total'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'details', name: 'details', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection
