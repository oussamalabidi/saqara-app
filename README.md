# Saqara test
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.



## Installation

require ^7.3|^8.0

Install the dependencies.

```sh
composer install
```

Generate app key
```sh
php artisan key:generate
```

Copy .env.example and than change your database config
```sh
cp .env.example .env
```

Run migrations
```sh
php artisan migrate
```

Run seeds
```sh
php artisan db:seed --class=UserSeeder
php artisan db:seed --class=ArticleSeeder
php artisan db:seed --class=OrdersSeeder
php artisan db:seed --class=OrderArticlesSeeder

```

Create admin user
```sh
php artisan add:admin
```

Installing Laravel Mix
```sh
npm install
```

Running Mix
```sh
npm run dev
```

Run the server
```sh
php artisan serve
```

**Login admin user**
```sh
admin@test.com/admin
```

Note: we can set docker envirment with Laravel Sail
