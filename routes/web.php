<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'authenticate'])->name('login');

Route::group(['middleware' => 'admin', 'prefix' => 'admin/'], function () {
    Route::post('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
    Route::get('customers', [App\Http\Controllers\Admin\CustomersController::class, 'index'])->name('customers');
    Route::get('export/customers', [App\Http\Controllers\Admin\ExportController::class, 'exportCustomers'])->name('export_customers');
    Route::get('orders', [App\Http\Controllers\Admin\OrdersController::class, 'index'])->name('orders');
});
